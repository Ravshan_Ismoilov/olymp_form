from django.contrib import admin
from import_export.admin import ImportExportModelAdmin, ExportActionModelAdmin
from import_export.fields import Field
from import_export import resources

from mainapp.models import Viloyat, Tuman, Maktab, Fanlar, Students, Natija, Totaldict


class ViloyatResource(resources.ModelResource):
    class Meta:
        model = Viloyat
        fields = ('id', 'title', 'status', 'creator_by__username', 'created', 'updated')
        export_order = ('id', 'title', 'status', 'created',)

@admin.register(Viloyat)
class ViloyatAdmin(ImportExportModelAdmin):
    resource_class = ViloyatResource
    list_display = ('pk', 'title', 'status', 'creator_by', 'created', 'updated')
    list_filter = ('status', 'created', 'updated')
    readonly_fields = ('creator_by', 'created', 'updated')

    def save_model(self, request, obj, form, change):
        obj.creator_by = request.user
        super().save_model(request, obj, form, change)


class TumanResource(resources.ModelResource):
    shundan = Field()
    class Meta:
        model = Tuman
        # fields = ('id', 'title', 'shundan', 'vil__title', 'status', 'creator_by__username', 'created', 'updated')
        export_order = ('id', 'title', 'shundan', 'created',)

    def dehydrate_shundan(self, obj):
        return obj.shundan()
    
@admin.register(Tuman)
class TumanAdmin(ImportExportModelAdmin):
    resource_class = TumanResource
    list_display = ('pk', 'title', 'jami', 'shundan', 'vil', 'status', 'creator_by', 'created', 'updated')
    list_filter = ('vil', 'status', 'created', 'updated')
    readonly_fields = ('creator_by', 'created', 'updated')
        
    def save_model(self, request, obj, form, change):
        obj.creator_by = request.user
        super().save_model(request, obj, form, change)

class MaktabResource(resources.ModelResource):
    jami = Field()
    class Meta:
        model = Maktab
        fields = ('id', 'title', 'jami', 'viloyat__title', 'tuman__title', 'created')
        # export_order = ('id', 'title', 'viloyat__title', 'tuman__title', 'created',)

    def dehydrate_jami(self, obj):
        return obj.jami()
    
@admin.register(Maktab)
class MaktabAdmin(ImportExportModelAdmin):
    resource_class = MaktabResource
    list_display = ('pk', 'title', 'jami', 'viloyat', 'tuman', 'status', 'creator_by', 'created', 'updated')
    list_filter = ('viloyat', 'tuman', 'status', 'created', 'updated')
    readonly_fields = ('creator_by', 'created', 'updated')
    
    def save_model(self, request, obj, form, change):
        obj.creator_by = request.user
        super().save_model(request, obj, form, change)

class StudentsResource(resources.ModelResource):
    class Meta:
        model = Students
        fields = ('id', 'fio', 'passport', 'lang', 'fan__title', 'viloyat__title', 'tuman__title', 'maktab__title', 'status', 'created', 'updated')
        export_order = ('id', 'fio', 'passport', 'lang', 'fan__title', 'tuman__title', 'created',)

@admin.register(Students)
class StudentsAdmin(ImportExportModelAdmin):
    resource_class = StudentsResource
    list_display = ( 'fio', 'passport', 'tuman', 'maktab', 'fan', 'lang', 'status', 'created', 'updated')
    list_filter = ('fan', 'lang', 'viloyat', 'tuman', 'created', 'status', 'updated')
    search_fields = ('fio', 'passport', 'maktab__title',)
    list_per_page = 10000

class FanlarResource(resources.ModelResource):
    umumiy = Field()
    uz = Field()
    ru = Field()

    class Meta:
        model = Fanlar
        fields = ('id', 'title', 'umumiy', 'uz', 'ru',)
        # export_order = ('id', 'title', 'umumiy', 'uz', 'ru',)

    def dehydrate_umumiy(self, obj):
        return obj.umumiy()
    
    def dehydrate_uz(self, obj):
        return obj.uz()

    def dehydrate_ru(self, obj):
        return obj.ru()
    
@admin.register(Fanlar)
class FanlarAdmin(ImportExportModelAdmin):
    resource_class = FanlarResource
    list_display = ('pk', 'title', 'umumiy', 'shundan', 'uz', 'ru', 'status', 'creator_by', 'created', 'updated')
    list_filter = ('status', 'created', 'updated')
    readonly_fields = ('creator_by', 'created', 'updated')

    def save_model(self, request, obj, form, change):
        obj.creator_by = request.user
        super().save_model(request, obj, form, change)

class NatijaResource(resources.ModelResource):
    class Meta:
        model = Natija
        fields = ('fio', 'fan', 'urni', 'ball', 'berilgan_vaqti', 'created',)

@admin.register(Natija)
class NatijaAdmin(ImportExportModelAdmin):
    resources_class = NatijaResource
    list_display = ('slug', 'fio', 'fan', 'urni', 'ball', 'berilgan_vaqt', 'status', 'img')
    fields = ('fio', 'fan', 'urni', 'ball', 'berilgan_vaqt', 'status', 'qr_code', 'diplom', 'diplom_seriyasi', 'sertificate', 'sertifikat_seriyasi', )
    readonly_field = ('slug', 'creator_by',  'created', 'updated', 'img')

    def save_model(self, request, obj, form, change):
        obj.creator_by = request.user
        super().save_model(request, obj, form, change)

class TotaldictResource(resources.ModelResource):
    class Meta:
        model = Totaldict
        fields = ('fio', 'phone', 'email', 'ish_joyi', 'lavozimi', 'turi', 'created', )

@admin.register(Totaldict)
class TotaldictAdmin(ImportExportModelAdmin):
    resources_class = TotaldictResource
    list_display = ('fio', 'phone', 'email', 'ish_joyi', 'lavozimi', 'turi', 'status', 'created', )
    # fields = ('fio', 'fan', 'urni', 'ball', 'berilgan_vaqt', 'status', 'qr_code', 'diplom', 'diplom_seriyasi', 'sertificate', 'sertifikat_seriyasi', )
    readonly_field = ('creator_by',  'created', 'updated')