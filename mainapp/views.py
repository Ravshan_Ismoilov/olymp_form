from django.shortcuts import render, get_object_or_404
from django.contrib import messages

from mainapp.models import Tuman, Maktab, Viloyat, Natija, Totaldict
from mainapp.forms import Register_form
from mainapp.utils import get_sertifikate

def index(request):
    success__ok = True
    vil = True
    if request.method == 'POST':
        register_form = Register_form(request.POST)
        if register_form.is_valid():
            register_form.save()
            success__ok = False
            messages.success(request, "Ваша данные получены.")
        else:
            messages.warning(request, "Произошла ошибка при сохранении данных. Пожалуйста, попробуйте еще раз.")
            print(register_form)
    else:
        register_form = Register_form()
    return render(request, 'totaldict.html', locals())

# def index(request):
#     print("123")
#     success__ok = True
#     vil = Viloyat.active.all()
#     if request.method == 'POST':
#         register_form = Register_form(request.POST)
#         if register_form.is_valid():
#             register_form.save()
#             success__ok = False
#             messages.success(request, "Ma'lumotlaringiz qabul qilindi.")
#         else:
#             messages.warning(request, "Ma'lumotlarni saqlashda xatolik sodir bo'ldi. Iltimos, qayta urunib ko'ring.")
#             print(register_form)
#     else:
#         register_form = Register_form()
#     return render(request, 'form.html', locals())

# def load_maktab(request):
#     if request.GET.get('tuman'):
#         tuman_id = request.GET.get('tuman')
#         maktab = Maktab.active.filter(tuman=tuman_id).order_by('pk').all()

#     if request.GET.get('viloyat'):
#         viloyat_id = request.GET.get('viloyat')
#         maktab = Tuman.active.filter(vil=viloyat_id).order_by('pk').all()

#     return render(request, 'drop_down_maktab.html', {'maktab': maktab})

# def check(request, slug=None):
    
#     try:
#         natija = get_object_or_404(Natija, slug=slug)
#     except:
#         messages.warning(request, "Siz mavjud bo'lmagan manzil kiritdingiz.")
#     # sert = get_sertifikate(natija)

#     return render(request, 'natija.html', locals())
