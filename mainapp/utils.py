from PIL import Image, ImageDraw, ImageFont
from django.core.files.base import ContentFile
from io import BytesIO
import os

BASE_DIR = os.getcwd()

def get_sertifikate(natija):
    cert_img = Image.open(BASE_DIR + natija.sertificate.url)
    qr_img = Image.open(BASE_DIR + natija.qr_code.url)

    new_image = Image.new('RGB', (cert_img.width, cert_img.height), (255, 255, 255))
    new_image.paste(cert_img, (0, 0))
    new_image.paste(qr_img, (3240, 2100))
    
    draw = ImageDraw.Draw(new_image)
    
    font = ImageFont.truetype('arial.ttf', 150)
    draw.text((1100,860), natija.fio, font=font, fill=(0,0,0))
    
    font = ImageFont.truetype('arial.ttf', 100)
    draw.text((2200,1360), natija.fan, font=font, fill=(0,0,0))
    
    name = f'/user_cert/{natija.fio}_{natija.slug}.png'
    new_image.save(BASE_DIR + '/media/' + name)
    
    return name
