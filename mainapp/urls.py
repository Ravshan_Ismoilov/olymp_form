from django.urls import path
from mainapp.views import index

app_name = 'mainapp'

urlpatterns = [
    # path('ajax/load-maktab/', load_maktab, name='ajax_load_maktab'),
    # path('check/<slug:slug>', check, name='check'),
    # path('check/', check, name='check'),
    path('', index, name="index" ),
]
