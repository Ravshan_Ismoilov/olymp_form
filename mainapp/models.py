from django.db import models
from django.contrib.auth import get_user_model
from django.utils.html import mark_safe

from django.core.files.base import ContentFile
from io import BytesIO
import qrcode

import uuid

User = get_user_model()

TURI_CHOICES = (
        ('trud', 'TRUD(РКИ)'),
        ('dictant', 'Диктант')
    )

STATUS_CHOICES = (
    ('active', 'Active'),
    ('inactive', 'Inactive'),
    ('delete', 'Deleted'))

SINF_CHOICES = (
    ('11-sinf', '11-sinf'),
    ('2-kurs', '2-kurs'),
    )

LANG_CHOICES = (
        ('uz', 'O\'zbek'),
        ('ru', 'Rus'),
    )


class ActiveManager(models.Manager):
	def get_queryset(self):
		return super().get_queryset().filter(status='active')

class Fanlar(models.Model):
    title = models.CharField(max_length=100, verbose_name="Fan nomi")
    creator_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="fan_create", verbose_name="Xodim")
    created = models.DateTimeField(auto_now_add=True, verbose_name="Qo'shilgan vaqti")
    updated = models.DateTimeField(auto_now=True, verbose_name="O'zgartirilgan vaqti")
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='active', verbose_name="Holati")
    objects = models.Manager()
    active = ActiveManager()

    def shundan(self):
        students = Students.objects.filter(fan=self.pk).count()
        return students
    
    def umumiy(self):
        return Students.objects.count()
    
    def ru(self):
        return Students.objects.filter(lang='ru', fan=self.pk).count()
    
    def uz(self):
        return Students.objects.filter(lang='uz', fan=self.pk).count()
    
    def __str__(self):
        return f'{self.title}'
    
class Viloyat(models.Model):
    title = models.CharField(max_length=100, verbose_name="Viloyat")
    creator_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="viloyat_create", verbose_name="Xodim")
    created = models.DateTimeField(auto_now_add=True, verbose_name="Qo'shilgan vaqti")
    updated = models.DateTimeField(auto_now=True, verbose_name="O'zgartirilgan vaqti")
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='active', verbose_name="Holati")
    objects = models.Manager()
    active = ActiveManager()

    def __str__(self):
        return f'{self.title}'

class Tuman(models.Model):
    vil = models.ForeignKey(Viloyat, on_delete=models.CASCADE, verbose_name="Viloyat")
    title = models.CharField(max_length=100, verbose_name="Tuman")
    creator_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="tuman_create", verbose_name="Xodim")
    created = models.DateTimeField(auto_now_add=True, verbose_name="Qo'shilgan vaqti")
    updated = models.DateTimeField(auto_now=True, verbose_name="O'zgartirilgan vaqti")
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='active', verbose_name="Holati")
    objects = models.Manager()
    active = ActiveManager()

    def __str__(self):
        return f'{self.title}'
    
    def shundan(self):
        return Students.objects.filter(tuman=self.pk).count()
    shundan.short_description = "Shundan"

    def jami(self):
        students = Students.objects.count()
        return students

class Maktab(models.Model):
    viloyat = models.ForeignKey(Viloyat, on_delete=models.CASCADE, verbose_name="Viloyat")
    tuman = models.ForeignKey(Tuman, on_delete=models.CASCADE, verbose_name="Tuman")
    title = models.CharField(max_length=100, verbose_name="Maktab")
    creator_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="maktab_create", verbose_name="Xodim")
    created = models.DateTimeField(auto_now_add=True, verbose_name="Qo'shilgan vaqti")
    updated = models.DateTimeField(auto_now=True, verbose_name="O'zgartirilgan vaqti")
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='active', verbose_name="Holati")
    objects = models.Manager()
    active = ActiveManager()

    def __str__(self):
        return f'{self.title}'
    
    def jami(self):
        return Students.objects.filter(maktab=self.pk).count()

class Students(models.Model):
    viloyat = models.ForeignKey(Viloyat, on_delete=models.CASCADE, verbose_name="Viloyat")
    tuman = models.ForeignKey(Tuman, on_delete=models.CASCADE, verbose_name="Tuman")
    maktab = models.ForeignKey(Maktab, on_delete=models.CASCADE, verbose_name="Maktab")
    sinf = models.CharField(max_length=10, choices=SINF_CHOICES, verbose_name="Sinf")
    fan = models.ForeignKey(Fanlar, on_delete=models.CASCADE, verbose_name="Fan")
    lang = models.CharField(max_length=10, choices=LANG_CHOICES, verbose_name="Til")
    fio = models.CharField(max_length=255, verbose_name="FIO")
    passport = models.CharField(max_length=255, verbose_name="Passport(JSHSHIR)", unique=True)
    created = models.DateTimeField(auto_now_add=True, verbose_name="Qo'shilgan vaqti")
    updated = models.DateTimeField(auto_now=True, verbose_name="O'zgartirilgan vaqti")
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='active', verbose_name="Holati")
    objects = models.Manager()
    active = ActiveManager()

    def __str__(self):
        return f'{self.fio}'

class Natija(models.Model):
    slug = models.UUIDField(default=uuid.uuid4, editable=False)
    fio = models.CharField(max_length=100, verbose_name="Talabaning FIOsi")
    fan = models.CharField(max_length=100, verbose_name="Yo'nalishi")
    urni = models.CharField(max_length=100, verbose_name="Egallagan o'rni")
    ball = models.CharField(max_length=100, verbose_name="To'plagan balli")
    berilgan_vaqt = models.DateTimeField(verbose_name="Berilgan vaqti")
    qr_code = models.ImageField(upload_to='qr_code/%Y/%m/%d/', blank=True)
    diplom = models.ImageField(upload_to='diplom/%Y/%m/%d/', blank=True)
    diplom_seriyasi = models.CharField(max_length=100, verbose_name="Diplom seriyasi")
    sertificate = models.ImageField(upload_to='sertificate/%Y/%m/%d/', blank=True, null=True)
    sertifikat_seriyasi = models.CharField(max_length=100, verbose_name="Sertifikat seriyasi", blank=True, null=True)
    creator_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="sert_create", verbose_name="Xodim")
    created = models.DateTimeField(auto_now_add=True, verbose_name="Qo'shilgan vaqti")
    updated = models.DateTimeField(auto_now=True, verbose_name="O'zgartirilgan vaqti")
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='active', verbose_name="Holati")
    objects = models.Manager()
    active = ActiveManager()

    def img(self):
        return mark_safe('\
				<a href="/media/{photo}" target="blank" alt="{name}"> \
					<img src="/media/{photo}" width="50" height="50" /> \
				</a>'.format(
					photo=self.qr_code,
					name=self.fio
				)
			)
    img.short_description = "QR kodi"

    def save(self, *args, **kwargs):
        if not self.qr_code:
            qr = qrcode.QRCode(
                version=9,
                error_correction=qrcode.constants.ERROR_CORRECT_L,
                box_size=10,
                border=4,
            )
            qr.add_data(f'https://olymp.tiu-edu.uz/check/{self.slug}')
            qr.make(fit=True)
            img = qr.make_image(fill_color="black", back_color="white")
            img_io = BytesIO()
            img.save(img_io, 'PNG')
            self.qr_code.save(f'{self.fio}_{self.slug}.jpg', ContentFile(img_io.getvalue()), save=True)
        return super().save(*args, **kwargs)

    def __str__(self):
        return f'{self.fio}'


class Totaldict(models.Model):
    fio = models.CharField(max_length=255, verbose_name="ФИО")
    phone = models.CharField(max_length=255, verbose_name="Телефон")
    email = models.EmailField(verbose_name="Е-маил")
    ish_joyi = models.CharField(max_length=255, verbose_name="Места работы")
    lavozimi = models.CharField(max_length=255, verbose_name="Должность")
    turi = models.CharField(max_length=10, choices=TURI_CHOICES, default='active', verbose_name="Вид")
    created = models.DateTimeField(auto_now_add=True, verbose_name="Qo'shilgan vaqti")
    updated = models.DateTimeField(auto_now=True, verbose_name="O'zgartirilgan vaqti")
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='trud', verbose_name="Holati")
    objects = models.Manager()
    active = ActiveManager()

    def __str__(self):
        return f'{self.fio}'