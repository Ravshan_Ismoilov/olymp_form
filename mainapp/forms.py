from django import forms
from mainapp.models import Students, Viloyat, Tuman, Maktab, SINF_CHOICES, LANG_CHOICES, Totaldict


class Register_form(forms.ModelForm):
    class Meta:
        model = Totaldict
        fields = ('fio', 'phone', 'email', 'ish_joyi', 'lavozimi', 'turi', )

# class Register_form(forms.ModelForm):
#     lang = forms.ChoiceField(label="Til", choices=LANG_CHOICES)
#     sinf = forms.ChoiceField(label="Sinf", choices=SINF_CHOICES)
#     class Meta:
#         model = Students
#         fields = ('viloyat', 'tuman', 'maktab', 'sinf', 'fan', 'lang', 'fio', 'passport')

    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)

    #     self.fields['viloyat'].queryset = Viloyat.active.all()
    #     self.fields['viloyat'].empty_label = "Tanlang..."
    #     self.fields['tuman'].queryset = Tuman.active.none()
    #     self.fields['tuman'].empty_label = "Tanlang..."
    #     self.fields['maktab'].queryset = Maktab.active.none()
    #     self.fields['maktab'].empty_label = "Tanlang..."
    #     self.fields['fan'].empty_label = "Tanlang..."
    #     self.fields['fio'].widget.attrs['placeholder'] = "FIO kiriting..."
    #     self.fields['passport'].widget.attrs['placeholder'] = "Shaxsni tasdiqlovchi hujjat seriyasi va raqamini kiriting..."
 
    #     if 'tuman' in self.data:
    #         try:
    #             tuman_id = int(self.data.get('tuman'))
    #             self.fields['maktab'].queryset = Maktab.active.filter(tuman=tuman_id).order_by('pk')
    #             self.fields['maktab'].empty_label = "Tanlang..."
    #         except (ValueError, TypeError):
    #             pass  # invalid input from the client; ignore and fallback to empty City queryset
    #     elif self.instance.pk:
    #         self.fields['maktab'].queryset = self.instance.tuman.maktab_set.order_by('pk')
    #         self.fields['maktab'].empty_label = "Tanlang..."

    #     if 'viloyat' in self.data:
    #         try:
    #             viloyat_id = int(self.data.get('viloyat'))
    #             self.fields['tuman'].queryset = Tuman.active.filter(vil=viloyat_id).order_by('pk')
    #             self.fields['tuman'].empty_label = "Tanlang..."
    #         except (ValueError, TypeError):
    #             pass  # invalid input from the client; ignore and fallback to empty City queryset
    #     elif self.instance.pk:
    #         self.fields['tuman'].queryset = self.instance.vil.viloyat_set.order_by('pk')
    #         self.fields['tuman'].empty_label = "Tanlang..."